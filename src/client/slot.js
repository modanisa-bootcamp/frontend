import axios from "axios";

function fetchSlotResponse() {
  const headers = {
    Accept: "application/json",
  };

  return axios
    .get(`/slots`, {
      baseURL: "http://a5fab56ff83bf47aa9bc1d0cbfeabe25-1354475447.eu-west-2.elb.amazonaws.com",
      headers,
    })
    .then((response) => response.data)
    .catch((e) => {
      throw new Error(JSON.stringify(e));
    });
}

export function getSlots() {
  return fetchSlotResponse();
}


